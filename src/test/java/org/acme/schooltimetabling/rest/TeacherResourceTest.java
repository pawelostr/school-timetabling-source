package org.acme.schooltimetabling.rest;

import static io.restassured.RestAssured.given;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import org.acme.schooltimetabling.domain.Teacher;
import org.junit.jupiter.api.Test;

import io.quarkus.test.junit.QuarkusTest;
import io.restassured.http.ContentType;

@QuarkusTest
public class TeacherResourceTest {

    @Test
    public void getAll() {
        List<Teacher> teacherList = given()
                .when().get("/teachers")
                .then()
                .statusCode(200)
                .extract().body().jsonPath().getList(".", Teacher.class);
        assertFalse(teacherList.isEmpty());
        Teacher firsTeacher = teacherList.get(0);
        assertEquals("Room A", firsTeacher.getName());
    }

    @Test
    void addAndRemove() {
        ArrayList<String> subjects3 = new ArrayList<String>();
        subjects3.add("Biology");
        subjects3.add("French");
        subjects3.add("History");
        Teacher room = given()
                .when()
                .contentType(ContentType.JSON)
                .body(new Teacher("Test room", LocalTime.of(8, 00), LocalTime.of(16, 30), subjects3))
                .post("/rooms")
                .then()
                .statusCode(201)
                .extract().as(Teacher.class);

        given()
                .when()
                .delete("/teachers/{id}", room.getId())
                .then()
                .statusCode(204);
    }

}
