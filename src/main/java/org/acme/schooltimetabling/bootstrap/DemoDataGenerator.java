package org.acme.schooltimetabling.bootstrap;

import java.time.DayOfWeek;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import javax.transaction.Transactional;

import org.acme.schooltimetabling.domain.Lesson;
import org.acme.schooltimetabling.domain.Room;
import org.acme.schooltimetabling.domain.Teacher;
import org.acme.schooltimetabling.domain.Timeslot;
import org.acme.schooltimetabling.persistence.LessonRepository;
import org.acme.schooltimetabling.persistence.RoomRepository;
import org.acme.schooltimetabling.persistence.TeacherRepository;
import org.acme.schooltimetabling.persistence.TimeslotRepository;
import org.eclipse.microprofile.config.inject.ConfigProperty;

import io.quarkus.runtime.StartupEvent;

@ApplicationScoped
public class DemoDataGenerator {

    @ConfigProperty(name = "timeTable.demoData", defaultValue = "SMALL")
    DemoData demoData;

    @Inject
    TimeslotRepository timeslotRepository;
    @Inject
    RoomRepository roomRepository;
    @Inject
    TeacherRepository teacherRepository;
    @Inject
    LessonRepository lessonRepository;

    @Transactional
    public void generateDemoData(@Observes StartupEvent startupEvent) {
        if (demoData == DemoData.NONE) {
            return;
        }

        List<Timeslot> timeslotList = new ArrayList<>(10);
        timeslotList.add(new Timeslot(DayOfWeek.MONDAY, LocalTime.of(8, 30), LocalTime.of(9, 30)));
        timeslotList.add(new Timeslot(DayOfWeek.MONDAY, LocalTime.of(9, 30), LocalTime.of(10, 30)));
        timeslotList.add(new Timeslot(DayOfWeek.MONDAY, LocalTime.of(10, 30), LocalTime.of(11, 30)));
        timeslotList.add(new Timeslot(DayOfWeek.MONDAY, LocalTime.of(13, 30), LocalTime.of(14, 30)));
        timeslotList.add(new Timeslot(DayOfWeek.MONDAY, LocalTime.of(14, 30), LocalTime.of(15, 30)));

        timeslotList.add(new Timeslot(DayOfWeek.TUESDAY, LocalTime.of(8, 30), LocalTime.of(9, 30)));
        timeslotList.add(new Timeslot(DayOfWeek.TUESDAY, LocalTime.of(9, 30), LocalTime.of(10, 30)));
        timeslotList.add(new Timeslot(DayOfWeek.TUESDAY, LocalTime.of(10, 30), LocalTime.of(11, 30)));
        timeslotList.add(new Timeslot(DayOfWeek.TUESDAY, LocalTime.of(13, 30), LocalTime.of(14, 30)));
        timeslotList.add(new Timeslot(DayOfWeek.TUESDAY, LocalTime.of(14, 30), LocalTime.of(15, 30)));
        timeslotList.add(new Timeslot(DayOfWeek.WEDNESDAY, LocalTime.of(8, 30), LocalTime.of(9, 30)));
        timeslotList.add(new Timeslot(DayOfWeek.WEDNESDAY, LocalTime.of(9, 30), LocalTime.of(10, 30)));
        timeslotList.add(new Timeslot(DayOfWeek.WEDNESDAY, LocalTime.of(10, 30), LocalTime.of(11, 30)));
        timeslotList.add(new Timeslot(DayOfWeek.WEDNESDAY, LocalTime.of(13, 30), LocalTime.of(14, 30)));
        timeslotList.add(new Timeslot(DayOfWeek.WEDNESDAY, LocalTime.of(14, 30), LocalTime.of(15, 30)));
        if (demoData == DemoData.LARGE) {

            timeslotList.add(new Timeslot(DayOfWeek.THURSDAY, LocalTime.of(8, 30), LocalTime.of(9, 30)));
            timeslotList.add(new Timeslot(DayOfWeek.THURSDAY, LocalTime.of(9, 30), LocalTime.of(10, 30)));
            timeslotList.add(new Timeslot(DayOfWeek.THURSDAY, LocalTime.of(10, 30), LocalTime.of(11, 30)));
            timeslotList.add(new Timeslot(DayOfWeek.THURSDAY, LocalTime.of(13, 30), LocalTime.of(14, 30)));
            timeslotList.add(new Timeslot(DayOfWeek.THURSDAY, LocalTime.of(14, 30), LocalTime.of(15, 30)));
            timeslotList.add(new Timeslot(DayOfWeek.FRIDAY, LocalTime.of(8, 30), LocalTime.of(9, 30)));
            timeslotList.add(new Timeslot(DayOfWeek.FRIDAY, LocalTime.of(9, 30), LocalTime.of(10, 30)));
            timeslotList.add(new Timeslot(DayOfWeek.FRIDAY, LocalTime.of(10, 30), LocalTime.of(11, 30)));
            timeslotList.add(new Timeslot(DayOfWeek.FRIDAY, LocalTime.of(13, 30), LocalTime.of(14, 30)));
            timeslotList.add(new Timeslot(DayOfWeek.FRIDAY, LocalTime.of(14, 30), LocalTime.of(15, 30)));
        }
        timeslotRepository.persist(timeslotList);

        List<Room> roomList = new ArrayList<>(3);
        roomList.add(new Room("Room A"));
        roomList.add(new Room("Room B"));
        roomList.add(new Room("Room C"));
        if (demoData == DemoData.LARGE) {
            roomList.add(new Room("Room D"));
            roomList.add(new Room("Room E"));
            roomList.add(new Room("Room F"));
        }
        roomRepository.persist(roomList);

        List<Teacher> teacherList = new ArrayList<>(3);

        ArrayList<String> subjects = new ArrayList<String>();
        subjects.add("Math");
        subjects.add("Geography");
        subjects.add("Chemistry");
        teacherList.add(new Teacher("A. Turing", LocalTime.of(8, 00), LocalTime.of(16, 30), subjects));

        ArrayList<String> subjects2 = new ArrayList<String>();
        subjects2.add("Physics");
        subjects2.add("English");
        subjects2.add("Spanish");
        teacherList.add(new Teacher("M. Curie", LocalTime.of(8, 00), LocalTime.of(12, 00), subjects2));
        ArrayList<String> subjects3 = new ArrayList<String>();
        subjects3.add("Biology");
        subjects3.add("French");
        subjects3.add("History");
        teacherList.add(new Teacher("C. Darwin", LocalTime.of(11, 00), LocalTime.of(18, 00), subjects3));
        // teacherList.add(new Teacher("I. Jone"));
        // teacherList.add(new Teacher("P. Cruz"));

        teacherRepository.persist(teacherList);

        List<Lesson> lessonList = new ArrayList<>();
        lessonList.add(new Lesson("Math", "9th grade"));
        lessonList.add(new Lesson("Math", "9th grade"));
        lessonList.add(new Lesson("Physics", "9th grade"));
        lessonList.add(new Lesson("Chemistry", "9th grade"));
        lessonList.add(new Lesson("Biology", "9th grade"));
        lessonList.add(new Lesson("History", "9th grade"));
        lessonList.add(new Lesson("English", "9th grade"));
        lessonList.add(new Lesson("English", "9th grade"));
        lessonList.add(new Lesson("Spanish", "9th grade"));
        lessonList.add(new Lesson("Spanish", "9th grade"));
        // if (demoData == DemoData.LARGE) {
        //     lessonList.add(new Lesson("Math", "9th grade"));
        //     lessonList.add(new Lesson("Math", "9th grade"));
        //     lessonList.add(new Lesson("Math", "9th grade"));
        //     lessonList.add(new Lesson("ICT", "9th grade"));
        //     lessonList.add(new Lesson("Physics", "M. Curie", "9th grade"));
        //     lessonList.add(new Lesson("Geography", "9th grade"));
        //     lessonList.add(new Lesson("Geology", "9th grade"));
        //     lessonList.add(new Lesson("History", "9th grade"));
        //     lessonList.add(new Lesson("English", "9th grade"));
        //     lessonList.add(new Lesson("Drama", "9th grade"));
        //     lessonList.add(new Lesson("Art", "S. Dali", "9th grade"));
        //     lessonList.add(new Lesson("Art", "S. Dali", "9th grade"));
        //     lessonList.add(new Lesson("Physical education", "C. Lewis", "9th grade"));
        //     lessonList.add(new Lesson("Physical education", "C. Lewis", "9th grade"));
        //     lessonList.add(new Lesson("Physical education", "C. Lewis", "9th grade"));
        // }

        lessonList.add(new Lesson("Math", "10th grade"));
        lessonList.add(new Lesson("Math", "10th grade"));
        lessonList.add(new Lesson("Math", "10th grade"));
        lessonList.add(new Lesson("Physics", "10th grade"));
        lessonList.add(new Lesson("Chemistry", "10th grade"));
        lessonList.add(new Lesson("French", "10th grade"));
        lessonList.add(new Lesson("Geography", "10th grade"));
        lessonList.add(new Lesson("History", "10th grade"));
        lessonList.add(new Lesson("English", "10th grade"));
        lessonList.add(new Lesson("Spanish", "10th grade"));
        // if (demoData == DemoData.LARGE) {
        //     lessonList.add(new Lesson("Math", "10th grade"));
        //     lessonList.add(new Lesson("Math", "10th grade"));
        //     lessonList.add(new Lesson("ICT", "10th grade"));
        //     lessonList.add(new Lesson("Physics", "M. Curie", "10th grade"));
        //     lessonList.add(new Lesson("Biology", "10th grade"));
        //     lessonList.add(new Lesson("Geology", "10th grade"));
        //     lessonList.add(new Lesson("History", "10th grade"));
        //     lessonList.add(new Lesson("English", "10th grade"));
        //     lessonList.add(new Lesson("English", "10th grade"));
        //     lessonList.add(new Lesson("Drama", "10th grade"));
        //     lessonList.add(new Lesson("Art", "S. Dali", "10th grade"));
        //     lessonList.add(new Lesson("Art", "S. Dali", "10th grade"));
        //     lessonList.add(new Lesson("Physical education", "C. Lewis", "10th grade"));
        //     lessonList.add(new Lesson("Physical education", "C. Lewis", "10th grade"));
        //     lessonList.add(new Lesson("Physical education", "C. Lewis", "10th grade"));

        //     lessonList.add(new Lesson("Math", "11th grade"));
        //     lessonList.add(new Lesson("Math", "11th grade"));
        //     lessonList.add(new Lesson("Math", "11th grade"));
        //     lessonList.add(new Lesson("Math", "11th grade"));
        //     lessonList.add(new Lesson("Math", "A. Turing", "11th grade"));
        //     lessonList.add(new Lesson("ICT", "A. Turing", "11th grade"));
        //     lessonList.add(new Lesson("Physics", "M. Curie", "11th grade"));
        //     lessonList.add(new Lesson("Chemistry", "M. Curie", "11th grade"));
        //     lessonList.add(new Lesson("French", "M. Curie", "11th grade"));
        //     lessonList.add(new Lesson("Physics", "M. Curie", "11th grade"));
        //     lessonList.add(new Lesson("Geography", "11th grade"));
        //     lessonList.add(new Lesson("Biology", "11th grade"));
        //     lessonList.add(new Lesson("Geology", "11th grade"));
        //     lessonList.add(new Lesson("History", "11th grade"));
        //     lessonList.add(new Lesson("History", "11th grade"));
        //     lessonList.add(new Lesson("English", "11th grade"));
        //     lessonList.add(new Lesson("English", "11th grade"));
        //     lessonList.add(new Lesson("English", "11th grade"));
        //     lessonList.add(new Lesson("Spanish", "11th grade"));
        //     lessonList.add(new Lesson("Drama", "11th grade"));
        //     lessonList.add(new Lesson("Art", "S. Dali", "11th grade"));
        //     lessonList.add(new Lesson("Art", "S. Dali", "11th grade"));
        //     lessonList.add(new Lesson("Physical education", "C. Lewis", "11th grade"));
        //     lessonList.add(new Lesson("Physical education", "C. Lewis", "11th grade"));
        //     lessonList.add(new Lesson("Physical education", "C. Lewis", "11th grade"));

        //     lessonList.add(new Lesson("Math", "A. Turing", "12th grade"));
        //     lessonList.add(new Lesson("Math", "A. Turing", "12th grade"));
        //     lessonList.add(new Lesson("Math", "A. Turing", "12th grade"));
        //     lessonList.add(new Lesson("Math", "A. Turing", "12th grade"));
        //     lessonList.add(new Lesson("Math", "A. Turing", "12th grade"));
        //     lessonList.add(new Lesson("ICT", "A. Turing", "12th grade"));
        //     lessonList.add(new Lesson("Physics", "M. Curie", "12th grade"));
        //     lessonList.add(new Lesson("Chemistry", "M. Curie", "12th grade"));
        //     lessonList.add(new Lesson("French", "M. Curie", "12th grade"));
        //     lessonList.add(new Lesson("Physics", "M. Curie", "12th grade"));
        //     lessonList.add(new Lesson("Geography", "12th grade"));
        //     lessonList.add(new Lesson("Biology", "12th grade"));
        //     lessonList.add(new Lesson("Geology", "12th grade"));
        //     lessonList.add(new Lesson("History", "12th grade"));
        //     lessonList.add(new Lesson("History", "12th grade"));
        //     lessonList.add(new Lesson("English", "12th grade"));
        //     lessonList.add(new Lesson("English", "12th grade"));
        //     lessonList.add(new Lesson("English", "12th grade"));
        //     lessonList.add(new Lesson("Spanish", "12th grade"));
        //     lessonList.add(new Lesson("Drama", "12th grade"));
        //     lessonList.add(new Lesson("Art", "S. Dali", "12th grade"));
        //     lessonList.add(new Lesson("Art", "S. Dali", "12th grade"));
        //     lessonList.add(new Lesson("Physical education", "C. Lewis", "12th grade"));
        //     lessonList.add(new Lesson("Physical education", "C. Lewis", "12th grade"));
        //     lessonList.add(new Lesson("Physical education", "C. Lewis", "12th grade"));
        // }

        Lesson lesson = lessonList.get(0);
        lesson.setTimeslot(timeslotList.get(0));
        lesson.setRoom(roomList.get(0));
        lesson.setTeacher(teacherList.get(0));

        lessonRepository.persist(lessonList);
    }

    public enum DemoData {
        NONE,
        SMALL,
        LARGE
    }

}
