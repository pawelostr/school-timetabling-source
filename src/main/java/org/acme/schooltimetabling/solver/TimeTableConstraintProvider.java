package org.acme.schooltimetabling.solver;

import java.time.Duration;

// import static org.optaplanner.core.api.score.stream.ConstraintCollectors.*;

import org.acme.schooltimetabling.domain.Lesson;
import org.optaplanner.core.api.score.buildin.hardsoft.HardSoftScore;
import org.optaplanner.core.api.score.stream.Constraint;
import org.optaplanner.core.api.score.stream.ConstraintFactory;
import org.optaplanner.core.api.score.stream.ConstraintProvider;
import org.optaplanner.core.api.score.stream.Joiners;

public class TimeTableConstraintProvider implements ConstraintProvider {

    @Override
    public Constraint[] defineConstraints(ConstraintFactory constraintFactory) {
        return new Constraint[] {
                // Hard constraints
                roomConflict(constraintFactory),
                teacherConflict(constraintFactory),
                studentGroupConflict(constraintFactory),
                teacherSubjects(constraintFactory),

                // Soft constraints
                teacherRoomStability(constraintFactory),
                groupRoomStability(constraintFactory),
                teacherTimeEfficiency(constraintFactory),
                classTimeEfficiency(constraintFactory),
                studentGroupSubjectVariety(constraintFactory),
                teacherSchedule(constraintFactory),
                teacherWorkTime(constraintFactory)
        };
    }

//     private HardSoftScore hardOne = HardSoftScore.ONE_HARD;
    private HardSoftScore teacherStabilityPenalty = HardSoftScore.of(0, 1);
    private HardSoftScore studentStabilityPenalty = HardSoftScore.of(0, 2);

    private HardSoftScore teacherTimeEfficiencyReward = HardSoftScore.of(0, 1);
    private HardSoftScore studentTimeEfficiencyReward = HardSoftScore.of(0, 1);

    private Duration delayBetweenSameSubject = Duration.ofMinutes(300);
    private HardSoftScore subjectVarietyPenalty = HardSoftScore.of(0, 1);
    private HardSoftScore teacherSchedulePenalty = HardSoftScore.of(0, 50);

    private Duration maxWorkTime = Duration.ofMinutes(300);
    private HardSoftScore workTimePenalty = HardSoftScore.of(0, 20);


    Constraint roomConflict(ConstraintFactory constraintFactory) {
        // A room can accommodate at most one lesson at the same time.
        return constraintFactory
                // Select each pair of 2 different lessons ...
                .forEachUniquePair(Lesson.class, 
                        Joiners.equal(Lesson::getTimeslot), 
                        Joiners.equal(Lesson::getRoom))
                // ... and penalize each pair with a hard weight.
                .penalize(HardSoftScore.ONE_HARD)
                .asConstraint("Room conflict");
    }

    Constraint teacherConflict(ConstraintFactory constraintFactory) {
        // A teacher can teach at most one lesson at the same time.
        return constraintFactory
                .forEachUniquePair(Lesson.class,
                        Joiners.equal(Lesson::getTimeslot),
                        Joiners.equal(Lesson::getTeacher))
                .penalize(HardSoftScore.ONE_HARD)
                .asConstraint("Teacher conflict");
    }

    Constraint studentGroupConflict(ConstraintFactory constraintFactory) {
        // A student can attend at most one lesson at the same time.
        return constraintFactory
                .forEachUniquePair(Lesson.class,
                        Joiners.equal(Lesson::getTimeslot),
                        Joiners.equal(Lesson::getStudentGroup))
                .penalize(HardSoftScore.ONE_HARD)
                .asConstraint("Student group conflict");
    }

    Constraint teacherSubjects(ConstraintFactory constraintFactory) {
        // Teacher can only teach given subjects
        return constraintFactory
                .forEach(Lesson.class)
                .filter(lesson1 -> {
                        Boolean ehehe = lesson1.getTeacher().getSubjects().contains(lesson1.getSubject());
                        return !ehehe;
                    })
                .penalize(HardSoftScore.ONE_HARD)
                .asConstraint("Teacher allowed subjects");
    }



// SOFT CONSTRAINTS
    Constraint teacherRoomStability(ConstraintFactory constraintFactory) {
        // A teacher prefers to teach in a single room.
        return constraintFactory
                .forEachUniquePair(Lesson.class,
                        Joiners.equal(Lesson::getTeacher))
                .filter((lesson1, lesson2) -> lesson1.getRoom() != lesson2.getRoom())
                .penalize(teacherStabilityPenalty)
                .asConstraint("Teacher room stability");
    }

    Constraint groupRoomStability(ConstraintFactory constraintFactory) {
        // A student group prefers to learn in a single room.
        return constraintFactory
                .forEachUniquePair(Lesson.class,
                        Joiners.equal(Lesson::getStudentGroup))
                .filter((lesson1, lesson2) -> lesson1.getRoom() != lesson2.getRoom())
                .penalize(studentStabilityPenalty)
                .asConstraint("Student group room stability");
    }

    Constraint teacherTimeEfficiency(ConstraintFactory constraintFactory) {
        // A teacher prefers to teach sequential lessons and dislikes gaps between lessons.
        return constraintFactory
                .forEach(Lesson.class)
                .join(Lesson.class, 
                        Joiners.equal(Lesson::getTeacher),
                        Joiners.equal((lesson) -> lesson.getTimeslot().getDayOfWeek()))
                .filter((lesson1, lesson2) -> {
                    Duration between = Duration.between(lesson1.getTimeslot().getEndTime(),
                            lesson2.getTimeslot().getStartTime());
                    return !between.isNegative() && between.compareTo(Duration.ofMinutes(30)) <= 0;
                })
                .reward(teacherTimeEfficiencyReward)
                .asConstraint("Teacher time efficiency");
    }

    Constraint classTimeEfficiency(ConstraintFactory constraintFactory) {
        // A class student group prefers to teach sequential lessons and dislikes gaps between lessons.
        return constraintFactory
                .forEach(Lesson.class)
                .join(Lesson.class, 
                        Joiners.equal(Lesson::getStudentGroup),
                        Joiners.equal((lesson) -> lesson.getTimeslot().getDayOfWeek()))
                .filter((lesson1, lesson2) -> {
                    Duration between = Duration.between(lesson1.getTimeslot().getEndTime(),
                            lesson2.getTimeslot().getStartTime());
                    return !between.isNegative() && between.compareTo(Duration.ofMinutes(30)) <= 0;
                })
                .reward(studentTimeEfficiencyReward)
                .asConstraint("Student group time efficiency");
    }

    Constraint studentGroupSubjectVariety(ConstraintFactory constraintFactory) {
        // A student group dislikes sequential lessons on the same subject.
        return constraintFactory
                .forEach(Lesson.class)
                .join(Lesson.class,
                        Joiners.equal(Lesson::getSubject),
                        Joiners.equal(Lesson::getStudentGroup),
                        Joiners.equal((lesson) -> lesson.getTimeslot().getDayOfWeek()))
                .filter((lesson1, lesson2) -> {
                    Duration between = Duration.between(lesson1.getTimeslot().getEndTime(),
                            lesson2.getTimeslot().getStartTime());
                    return !between.isNegative() && between.compareTo(delayBetweenSameSubject) <= 0;
                })
                .penalize(subjectVarietyPenalty)
                .asConstraint("Student group subject variety");
    }

    Constraint teacherSchedule(ConstraintFactory constraintFactory) {
        // Teacher has time constraints
        return constraintFactory
                .forEach(Lesson.class)
                .filter(lesson1 -> {
                        Duration between_start = Duration.between(lesson1.getTeacher().getStartTime(),
                                lesson1.getTimeslot().getStartTime());
                        Duration between_end = Duration.between(lesson1.getTimeslot().getEndTime(),
                                lesson1.getTeacher().getEndTime());
                        return between_start.isNegative() || between_end.isNegative();
                    })
                .penalize(teacherSchedulePenalty)
                .asConstraint("Teacher time constraints");
    }

//     Constraint teacherWorkTime(ConstraintFactory constraintFactory) {
//         // Teacher doesn't want a single lesson in a day
//         return constraintFactory
//         .forEach(Lesson.class)
//         .join(Lesson.class, 
//                 Joiners.equal(Lesson::getTeacher),
//                 Joiners.equal((lesson) -> lesson.getTimeslot().getDayOfWeek()))
//         .reward(HardSoftScore.ONE_SOFT)
//         .asConstraint("Teacher leassons in a day");
//     }

    Constraint teacherWorkTime(ConstraintFactory constraintFactory) {
        // 
        return constraintFactory
        .forEachUniquePair(Lesson.class,
                        Joiners.equal(Lesson::getTeacher),
                        Joiners.equal((lesson) -> lesson.getTimeslot().getDayOfWeek()))
        .filter((lesson, lesson2) -> {
                Duration between_start = Duration.between(lesson.getTimeslot().getStartTime(),
                        lesson2.getTimeslot().getEndTime());
                Duration between_end = Duration.between(lesson2.getTimeslot().getStartTime(),
                        lesson.getTimeslot().getEndTime());
                return between_start.compareTo(maxWorkTime) >= 0 || between_end.compareTo(maxWorkTime) >= 0;
        } )
        .penalize(workTimePenalty)
        .asConstraint("Teacher leassons in a day");
    }




}
