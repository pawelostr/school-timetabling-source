package org.acme.schooltimetabling.domain;

import java.time.LocalTime;
import java.util.ArrayList;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.optaplanner.core.api.domain.lookup.PlanningId;

@Entity
public class Teacher {

    @PlanningId
    @Id
    @GeneratedValue
    private Long id;

    private String name;

    private LocalTime startTime;
    private LocalTime endTime;

    private ArrayList<String> subjects = new ArrayList<String>();

    // No-arg constructor required for Hibernate
    public Teacher() {
    }

    public Teacher(String name, LocalTime startTime, LocalTime endTime, ArrayList<String> subjects) {
        this.name = name;
        this.startTime = startTime;
        this.endTime = endTime;
        this.subjects = subjects;
    }

    // TODO: subjects they can teach, constraints (time etc)

    public Teacher(long id, String name, LocalTime startTime, LocalTime endTime, ArrayList<String> subjects) {
        this(name, startTime, endTime, subjects);
        this.id = id;
    }

    @Override
    public String toString() {
        return name;
    }

    // ************************************************************************
    // Getters and setters
    // ************************************************************************

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public LocalTime getStartTime() {
        return startTime;
    }

    public LocalTime getEndTime() {
        return endTime;
    }

    public ArrayList<String> getSubjects() {
        return subjects;
    }

}
