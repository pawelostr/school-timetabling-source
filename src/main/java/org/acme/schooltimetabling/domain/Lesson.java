package org.acme.schooltimetabling.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import org.optaplanner.core.api.domain.entity.PlanningEntity;
import org.optaplanner.core.api.domain.lookup.PlanningId;
import org.optaplanner.core.api.domain.variable.PlanningVariable;

@PlanningEntity
@Entity
public class Lesson {

    @PlanningId
    @Id
    @GeneratedValue
    private Long id;

    private String subject;
    public String teacher_name;
    private String studentGroup;

    @PlanningVariable(valueRangeProviderRefs = "timeslotRange")
    @ManyToOne
    private Timeslot timeslot;
    @PlanningVariable(valueRangeProviderRefs = "roomRange")
    @ManyToOne
    private Room room;
    @PlanningVariable(valueRangeProviderRefs = "teacherRange")
    @ManyToOne
    private Teacher teacher;

    // No-arg constructor required for Hibernate and OptaPlanner
    public Lesson() {
    }

    public Lesson(String subject, String studentGroup) {
        this.subject = subject;
        this.studentGroup = studentGroup;
    }

    public Lesson(long id, String subject, String studentGroup, Timeslot timeslot, Room room, Teacher teacher) {
        this(subject, studentGroup);
        this.id = id;
        this.timeslot = timeslot;
        this.room = room;
        this.teacher = teacher;
        // setTeacherName();
        // System.out.println(teacher.getName());
    }

    // public String setTeacherName() {
    //     teacher_name = "kurwa";
    //     return teacher_name;
    // }

    @Override
    public String toString() {
        return subject + "(" + id + ")";
    }

    // ************************************************************************
    // Getters and setters
    // ************************************************************************

    public Long getId() {
        return id;
    }

    public Integer get1() {
        return 1;
    }

    public String getSubject() {
        return subject;
    }

    public Teacher getTeacher() {
        return teacher;
    }

    public String getStudentGroup() {
        return studentGroup;
    }

    public Timeslot getTimeslot() {
        return timeslot;
    }

    public void setTimeslot(Timeslot timeslot) {
        this.timeslot = timeslot;
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    public void setTeacher(Teacher teacher) {
        this.teacher = teacher;
        this.teacher_name = teacher.getName();
    }
}
